package com.flytxt.api.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HTTP;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.flytxt.api.clientAES.AESClient;
import com.flytxt.api.service.EncryptionAesService;

@Service
public class EncryptionAesImpl implements EncryptionAesService {

	private Logger log = Logger.getLogger(EncryptionAesImpl.class);

	@Autowired
	private Environment environment;

	private static String responseData1 = null;
	
	private static String msisdn;

	@Override
	public String encrypt(String request, HttpServletRequest requestHeader) throws Exception {

		request = request.replaceAll("\\s+", "");
		//request= request.replace("</ID></REQUEST>", "</ID> </REQUEST>");
		Map<String, String> map = new HashMap<>();
		map.put("user-agent", requestHeader.getHeader("user-agent"));
		map.put("Host", requestHeader.getHeader("Host"));
		map.put("Authorization", requestHeader.getHeader("Authorization"));
		map.put("Cache-Control", requestHeader.getHeader("Cache-Control"));
		map.put("Content-Length", requestHeader.getHeader("Content-Length"));
		map.put("Content-Type", requestHeader.getHeader("Content-Type"));
		Pattern pattern = Pattern.compile("<MSISDN>(.*?)</MSISDN>");
		Matcher matcher = pattern.matcher(request);
		while (matcher.find()) {
		    msisdn =(matcher.group(1));
		}
		log.info(msisdn+" REQUEST: " + request);
		String encryptedData = "data=" + AESClient.Encrypt(request);
		log.info(msisdn +" EncryptedData: " + encryptedData);
		String responseData = sendEncryptedRequest(encryptedData, map);
		return responseData;

	}

	private String sendEncryptedRequest(String request, Map<String, String> map) {

		try {
			System.out.println("Content-Length: "+String.valueOf(request.length()));
			String uri = environment.getProperty("urlToSend");
			String host =environment.getProperty("Host");
			String contentType=environment.getProperty("ContentType");
			

			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(uri);
			
			

			// add header
			post.setHeader(HTTP.TARGET_HOST, host);
			post.setHeader("Authorization", map.get("Authorization"));
			post.setHeader("Cache-Control", map.get("Cache-Control"));
//			post.setHeader(HTTP.CONTENT_LEN, String.valueOf(request.length()));
			post.setHeader(HTTP.CONTENT_TYPE, contentType);
			post.setHeader("charset", "utf-8");
			post.setHeader("Accept", "text/plain");
			
			post.setEntity(new StringEntity(request));

			HttpResponse response = client.execute(post);
			System.out.println("\nSending 'POST' request to URL : " + uri);
			System.out.println("Response Code : " +
	                                    response.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(
	                        new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			log.info(msisdn+"Response:"+result.toString());
			
//			URL obj = new URL(uri);
//			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//			con.setRequestMethod("POST");
//			con.setRequestProperty("Host", host);
//			con.setRequestProperty("Authorization", map.get("Authorization"));
//			con.setRequestProperty("Cache-Control", map.get("Cache-Control"));
//			con.setRequestProperty("Content-Length", String.valueOf(request.length()));
//			con.setRequestProperty("Content-Type", contentType);
//			con.setRequestProperty("charset", "utf-8");
//			con.setRequestProperty("Accept", "text/plain");
//			log.info("Host:"+host);
//			log.info("Content-Length:"+String.valueOf(request.length()));
//			log.info("Content-Type:"+contentType);
//			con.setDoInput(true);
//			con.setDoOutput(true);
//			con.setUseCaches(false);
//			con.setAllowUserInteraction(false);
//			con.setDoOutput(true);
//			
//			// Send request
//			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//			wr.writeBytes(request);
//			wr.flush();
//			wr.close();
//			
//			// Get Response
//			InputStream is = con.getInputStream();
//			if (con.getResponseCode() != 200) {
//				log.debug(msisdn+" error code not 200");
//				throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
//			}
//			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//			String line = "";
//			StringBuilder response = new StringBuilder();
//			log.info("ResponseCode: " + con.getResponseCode());
//			while ((line = rd.readLine()) != null) {
//				response.append(line);
//				response.append('\r');
//			}
			responseData1 = result.toString();

		} catch (Exception e) {
			e.printStackTrace();
			log.error(msisdn+" Exception in sendEncryptedRequest" + e);
		}
		return responseData1;
	}

}

