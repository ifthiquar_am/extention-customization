
import java.io.UnsupportedEncodingException;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.RijndaelEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.paddings.ZeroBytePadding;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.encoders.Base64;

import com.terra.carrierbilling.ConfigData;

public class AES {
	private static byte[] _sessionKey = "YOUR-KEY".getBytes(); 
	private static byte[] _initialVector = "TerraNetworksMEX".getBytes();
    	private static int _keySize = 256;
    	private static int _blockSize = 128;	
	
	public static String Encrypt(String txt)
	{
		PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CBCBlockCipher(new RijndaelEngine(_blockSize)), new ZeroBytePadding());
		CipherParameters ivAndKey = new ParametersWithIV(new KeyParameter(_sessionKey, 0, (_keySize / 8)), _initialVector, 0, (_blockSize / 8));
		cipher.init(true, ivAndKey);

		try {
			byte[] decoded = txt.trim().getBytes("UTF-8");
			byte[] encoded  = new byte[cipher.getOutputSize(decoded.length)];
			int len = cipher.processBytes(decoded, 0, decoded.length, encoded, 0);
			
			cipher.doFinal(encoded, len);
			return new String(Base64.encode(encoded));
		} 
		catch (DataLengthException | IllegalStateException | InvalidCipherTextException | UnsupportedEncodingException e) {
			return null;
		}
	}
	
	public static String Decrypt(String txt)
	{
	    PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CBCBlockCipher(new RijndaelEngine(_blockSize)), new ZeroBytePadding());
		CipherParameters ivAndKey = new ParametersWithIV(new KeyParameter(_sessionKey, 0, (_keySize / 8)), _initialVector, 0, (_blockSize / 8));	    
		cipher.init(false, ivAndKey);	    

	    try {
		    byte[] encoded = Base64.decode(txt.trim());
		    byte[] decoded = new byte[cipher.getOutputSize(encoded.length)];
		    int len = cipher.processBytes(encoded, 0, encoded.length, decoded, 0);
		    
			cipher.doFinal(decoded, len);
			return (new String(decoded, "UTF-8")).trim();
		} 
	    catch (DataLengthException | IllegalStateException | InvalidCipherTextException | UnsupportedEncodingException e) 
	    {
			//e.printStackTrace();
	    	return null;
		}
	}
}
