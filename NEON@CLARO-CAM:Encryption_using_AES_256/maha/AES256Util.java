package com.flytxt.encryptor.util;

import java.io.UnsupportedEncodingException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.flytxt.security.EncryptionException;


/**
 * Password based algorithm based on Advanced Encryption Standard (AES) used for encryption/decryption of msisdn.
 */
public class AES256Util {

	private static final byte[] SALT = { (byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12, (byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12 };

	private static String iv = "8888888888888888";

	private IvParameterSpec ivSpec;

	private Cipher encryptCipher;

	private Cipher decryptCipher;

	public AES256Util() throws EncryptionException {
		try {
			this.ivSpec = new IvParameterSpec(iv.getBytes("UTF-8"));
		}
		catch (UnsupportedEncodingException e) {
			throw new EncryptionException(e);
		}
	}


	public String encrypt(String inputText) throws EncryptionException {
		try {
			byte[] ciphertext = encryptCipher.doFinal(inputText.getBytes("UTF-8"));

			return new String(Base64.encodeBase64(ciphertext, false, true));
		}
		catch (Exception e) {
			throw new EncryptionException(e);
		}
	}

	public String decrypt(String encriptedText) throws EncryptionException {
		try {
			byte[] raw = Base64.decodeBase64(encriptedText);

			byte[] stringBytes = decryptCipher.doFinal(raw);

			return new String(stringBytes, "UTF-8");
		}
		catch (Exception e) {
			throw new EncryptionException(e);
		}
	}

	public void setEncryptCipher(String key) throws EncryptionException {
		try {
			encryptCipher = getCipher(key, Cipher.ENCRYPT_MODE);
		}
		catch (Exception e) {
			throw new EncryptionException(e);
		}
	}

	public void setDecryptCipher(String key) throws EncryptionException {
		try {
			decryptCipher = getCipher(key, Cipher.DECRYPT_MODE);
		}
		catch (Exception e) {
			throw new EncryptionException(e);
		}
	}

	private Cipher getCipher(String password, int mode) throws Exception {
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(password.toCharArray(), SALT, 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

		cipher.init(mode, secret, ivSpec);

		return cipher;
	}
}
