import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public class EncryptedTokenTNPS {


    private static final String SHARED_KEY = "Mv@ppV0d@f0n3k3y"; 

    private static final String PREFIX = "VODATNPS";

    public static void main(String[] args) throws Exception {
        // get the user token from the input
        String userToken = args[0];

	long ts = System.currentTimeMillis() / 1000L;
        // create the user token
        String token = PREFIX + "|" + userToken + "|" + ts; 

        System.out.println("Input Values");
        System.out.println("UserToken = "+userToken);
        System.out.println("Token Generated at timestamp = "+ ts);

	System.out.println("String that will be encrypted = "+token);

        // get the encrypted token
        String encrypted_token = encrypt(token);
        System.out.println("Encrypted token " + encrypted_token);
        
        String decrypted_token = decrypt(encrypted_token);
        System.out.println("Decrypted token " + decrypted_token);
    }
 
    private static String encrypt(String string) throws Exception {
        byte[] keyData = (SHARED_KEY).getBytes();

        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "key#sec@");
        Cipher cipher = Cipher.getInstance("key#sec@");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        byte[] hasil = cipher.doFinal(string.getBytes());
        byte[] encodedBytes = Base64.encodeBase64(hasil, false, true);

        return new String(encodedBytes);
    }
     
    private static String decrypt(String string) throws Exception {
        byte[] keyData = (SHARED_KEY).getBytes();
        
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "key#sec@");
        Cipher cipher = Cipher.getInstance("key#sec@");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec); 
        byte[] hasil = cipher.doFinal(Base64.decodeBase64(string.getBytes()));

        return new String(hasil);
    }
}

