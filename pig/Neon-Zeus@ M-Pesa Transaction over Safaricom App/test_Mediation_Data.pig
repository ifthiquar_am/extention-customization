Register /sftp/CIE/WORKFLOWS/UDF/udf.jar;
REGISTER file:/usr/local/pig/contrib/piggybank/java/piggybank.jar;
DEFINE UnixToISO org.apache.pig.piggybank.evaluation.datetime.convert.UnixToISO();

rmf '/tmp/data_rev'

data_load = load '/CIE/PYTHON_UPLOADER/RTE_Data/DATA' using PigStorage(',');
mon_load = load '/CIE/PYTHON_UPLOADER/RTE_Data/MON' using PigStorage(',');
data_bundle_product_load = load '/CIE/Mediation_Data/ProductList.txt' using PigStorage(',');
mgr_load = load '/CIE/PYTHON_UPLOADER/RTE_Data/MGR' using PigStorage(',');
vou_load = load '/CIE/PYTHON_UPLOADER/RTE_Data/VOU' using PigStorage(',');
mpesa_load = load '/CIE/PYTHON_UPLOADER/RTE_Data/MPESA' using org.apache.pig.piggybank.storage.CSVExcelStorage('|');


-------------------Data------------------------------------------------------
data_gen_tmp =   foreach data_load generate (chararray)SUBSTRING($0,0,8) as date,
                                        (chararray)((TRIM($1) matches '^254.*' AND SIZE(TRIM($1)) == 12) ? TRIM($1) : ((TRIM($1) matches '^007.*' AND SIZE(TRIM($1)) == 11) ? CONCAT('254', SUBSTRING(TRIM($1), 2, 11)) : ((TRIM($1) matches '07.*' AND SIZE(TRIM($1)) == 10) ? CONCAT('254',SUBSTRING(TRIM($1), 1, 10)) : ((TRIM($1) matches '7.*' AND SIZE(TRIM($1)) == 9) ? CONCAT('254', TRIM($1)) : 'NA' )))) as CallingPartyNumber,
										(long)$2 as TotalFlux_1,
										(chararray)((TRIM($4) matches '^'254'.*' AND SIZE(TRIM($4)) == 12) ? TRIM($4) : ((TRIM($4) matches '^007.*' AND SIZE(TRIM($4)) == 11) ? CONCAT('254', SUBSTRING(TRIM($4), 2, 11)) : ((TRIM($4) matches '07.*' AND SIZE(TRIM($4)) == 10) ? CONCAT('254',SUBSTRING(TRIM($4), 1, 10)) : ((TRIM($4) matches '7.*' AND SIZE(TRIM($4)) == 9) ? CONCAT('254', TRIM($4)) : 'NA' )))) as msisdn,
										(long)$9 as TotalFlux_2,
										(chararray)$15 as GPPRATType,
										(long)$17 as unit_1,
                 						(long)$18 as unit_2;
                                        
data_gen = filter data_gen_tmp by not(msisdn == 'NA') or not(CallingPartyNumber == 'NA');
										
---------------Total-Data-Usage--------------------------------------------------------------------------------------------------------
data_gen_7 = foreach data_gen generate CallingPartyNumber, date, (double)TotalFlux_1/(1024*1024);
data_grp_2 = group data_gen_7 by (CallingPartyNumber, date);
data_usage = foreach data_grp_2 generate flatten(group), (double)com.flytxt.pig.udf.Round((SUM(data_gen_7.$2) IS NOT NULL ? SUM(data_gen_7.$2) : 0.0), 2);
                                        
								
								
--------------------------Paid_Data_Usage-----------------------------------------------------------------------------------------------
data_gen_3 = foreach data_gen generate msisdn, date, (double)((TotalFlux_1-TotalFlux_2)/(1024*1024));
data_grp_3 = group data_gen_3 by (msisdn, date);
paid_data_usage = foreach data_grp_3 generate flatten(group), (double)com.flytxt.pig.udf.Round((SUM(data_gen_3.$2) IS NOT NULL ? SUM(data_gen_3.$2) : 0.0), 2);


-----------------------Bundle Usage------------------------------------------------------------------------------------------------------
data_grp_4 = group data_gen by (CallingPartyNumber, date);
Bundle_data_usage = foreach data_grp_4 generate flatten(group), (double)com.flytxt.pig.udf.Round((SUM(data_gen.$4) IS NOT NULL ? (double)SUM(data_gen.$4)/(1024*1024) : 0.0), 2);

--------------------2G_Data_Usage--------------------------------------------------------------------------------------------------------
Data2G = filter data_gen by GPPRATType == '2';
data_grp_5 = group Data2G by (msisdn, date);
data_usage_2G = foreach data_grp_5 generate flatten(group), (double)com.flytxt.pig.udf.Round((SUM(Data2G.$2) IS NOT NULL ? (double)SUM(Data2G.$2)/(1024*1024) : 0.0), 2);


--------------------3G Data Usage----------------------------------------------------------------------------------------------------------
Data3G = filter data_gen by GPPRATType == '1';
data_grp_6 = group Data3G by (msisdn, date);
data_usage_3G = foreach data_grp_6 generate flatten(group), (double)com.flytxt.pig.udf.Round((SUM(Data3G.$2) IS NOT NULL ? (double)SUM(Data3G.$2)/(1024*1024) : 0.0), 2);

--------------------4G Data Usage----------------------------------------------------------------------------------------------------------
Data4G = filter data_gen by GPPRATType == '6';
data_grp_7 = group Data4G by (msisdn, date);
data_usage_4G = foreach data_grp_7 generate flatten(group), (double)com.flytxt.pig.udf.Round((SUM(Data4G.$2) IS NOT NULL ? (double)SUM(Data4G.$2)/(1024*1024) : 0.0), 2);


---------------DATA-REVENUE---------------
data_gen_1_tmp = foreach data_load generate (chararray)((TRIM($4) matches '^254.*' AND SIZE(TRIM($4)) == 12) ? TRIM($4) : ((TRIM($4) matches '^007.*' AND SIZE(TRIM($4)) == 11) ? CONCAT('254', SUBSTRING(TRIM($4), 2, 11)) : ((TRIM($4) matches '07.*' AND SIZE(TRIM($4)) == 10) ? CONCAT('254',SUBSTRING(TRIM($4), 1, 10)) : ((TRIM($4) matches '7.*' AND SIZE(TRIM($4)) == 9) ? CONCAT('254', TRIM($4)) : 'NA' )))) as msisdn,
                                        (chararray)SUBSTRING($0,0,8) as date,
                                        (long)$17 as unit_1;

data_gen_2_tmp = foreach data_load generate (chararray)((TRIM($4) matches '^254.*' AND SIZE(TRIM($4)) == 12) ? TRIM($4) : ((TRIM($4) matches '^007.*' AND SIZE(TRIM($4)) == 11) ? CONCAT('254', SUBSTRING(TRIM($4), 2, 11)) : ((TRIM($4) matches '07.*' AND SIZE(TRIM($4)) == 10) ? CONCAT('254',SUBSTRING(TRIM($4), 1, 10)) : ((TRIM($4) matches '7.*' AND SIZE(TRIM($4)) == 9) ? CONCAT('254', TRIM($4)) : 'NA' )))) as msisdn,
                                        (chararray)SUBSTRING($0,0,8) as date,
                                        (long)$18 as unit_2;

data_gen_1 = filter data_gen_1_tmp by not(msisdn == 'NA');
data_gen_2 = filter data_gen_2_tmp by not(msisdn == 'NA');
data_union = UNION data_gen_1, data_gen_2;
data_grp = group data_union by (msisdn, date);
data_rev = foreach data_grp generate flatten(group), (double)com.flytxt.pig.udf.Round((SUM(data_union.$2) IS NOT NULL ? SUM(data_union.$2)/10000 : 0.0), 2);

---------------DATA-BUNDLE-REVENUE---------------
data_bundle_product_gen = foreach data_bundle_product_load generate (chararray)$0 as data_bundle_product;
mon_gen_tmp = foreach mon_load generate (chararray)((TRIM($2) matches '^254.*' AND SIZE(TRIM($2)) == 12) ? TRIM($2) : ((TRIM($2) matches '^007.*' AND SIZE(TRIM($2)) == 11) ? CONCAT('254', SUBSTRING(TRIM($2), 2, 11)) : ((TRIM($2) matches '07.*' AND SIZE(TRIM($2)) == 10) ? CONCAT('254',SUBSTRING(TRIM($2), 1, 10)) : ((TRIM($2) matches '7.*' AND SIZE(TRIM($2)) == 9) ? CONCAT('254', TRIM($2)) : 'NA' )))) as msisdn_1,
                                    (chararray)((TRIM($11) matches '^254.*' AND SIZE(TRIM($11)) == 12) ? TRIM($11) : ((TRIM($11) matches '^007.*' AND SIZE(TRIM($11)) == 11) ? CONCAT('254', SUBSTRING(TRIM($11), 2, 11)) : ((TRIM($11) matches '07.*' AND SIZE(TRIM($11)) == 10) ? CONCAT('254',SUBSTRING(TRIM($11), 1, 10)) : ((TRIM($11) matches '7.*' AND SIZE(TRIM($11)) == 9) ? CONCAT('254', TRIM($11)) : 'NA' )))) as msisdn_2,
                                    (chararray)SUBSTRING($0,0,8) as date,
                                    (chararray)$4 as data_bundle_product,
                                    (int)$10 as charge_party_indicator,
                                    (int)$12 as unit_1,
                                    (int)$13 as unit_2;
mon_gen =  filter mon_gen_tmp by not(msisdn_1 == 'NA') or not(msisdn_2 == 'NA');                                  
mon_gen_2 = foreach mon_gen generate (charge_party_indicator == 9 ? msisdn_2 : msisdn_1) as msisdn, date, unit_1+unit_2;
mon_grp = group mon_gen_2 by (msisdn, date);
mon_rev = foreach mon_grp generate flatten(group), (double)com.flytxt.pig.udf.Round((SUM(mon_gen_2.$2) IS NOT NULL ? SUM(mon_gen_2.$2)/10000 : 0.0), 2);

---------------DAILY-BUNDLE-REVENUE---------------
mgr_filter = filter mgr_load by (($2 matches '4055038' OR $2 matches '4055999') AND ($10 matches '@ mb' OR $10 matches '@ gb'));
mgr_gen_tmp = foreach mgr_filter generate (chararray)((TRIM($1) matches '^254.*' AND SIZE(TRIM($1)) == 12) ? TRIM($1) : ((TRIM($1) matches '^007.*' AND SIZE(TRIM($1)) == 11) ? CONCAT('254', SUBSTRING(TRIM($1), 2, 11)) : ((TRIM($1) matches '07.*' AND SIZE(TRIM($1)) == 10) ? CONCAT('254',SUBSTRING(TRIM($1), 1, 10)) : ((TRIM($1) matches '7.*' AND SIZE(TRIM($1)) == 9) ? CONCAT('254', TRIM($1)) : 'NA' )))) as msisdn,
                                      (chararray)SUBSTRING($0,0,8) as date,
                                      (long)$7 as unit_1,
                                      (long)$8 as unit_2;
mgr_gen =  filter mgr_gen_tmp by not(msisdn == 'NA');
mgr_gen_2 = foreach mgr_gen generate msisdn, date, unit_1+unit_2;
mgr_grp = group mgr_gen_2 by (msisdn, date);
mgr_rev = foreach mgr_grp generate flatten(group), (double)com.flytxt.pig.udf.Round((SUM(mgr_gen_2.$2) IS NOT NULL ? SUM(mgr_gen_2.$2)/10000 : 0.0), 2);


---------------DIRECT-DATA-BUNDLE-REVENUE---------------
vou_filter = filter vou_load by ($6 matches '1007' OR $9 matches '5011');
vou_gen_tmp = foreach vou_filter generate (chararray)((TRIM($1) matches '^254.*' AND SIZE(TRIM($1)) == 12) ? TRIM($1) : ((TRIM($1) matches '^007.*' AND SIZE(TRIM($1)) == 11) ? CONCAT('254', SUBSTRING(TRIM($1), 2, 11)) : ((TRIM($1) matches '07.*' AND SIZE(TRIM($1)) == 10) ? CONCAT('254',SUBSTRING(TRIM($1), 1, 10)) : ((TRIM($1) matches '7.*' AND SIZE(TRIM($1)) == 9) ? CONCAT('254', TRIM($1)) : 'NA' )))) as msisdn,
                                      (chararray)SUBSTRING($0,0,8) as date,
                                      (long)$10 as unit;
vou_gen = filter vou_gen_tmp by not(msisdn == 'NA');                                  
vou_grp = group vou_gen by (msisdn, date);
vou_rev = foreach vou_grp generate flatten(group), (double)com.flytxt.pig.udf.Round((SUM(vou_gen.$2) IS NOT NULL ? SUM(vou_gen.$2)/10000 : 0.0), 2);


---------------MPESA-REVENUE---------------
mpesa_filter = filter mpesa_load by ($10 matches 'everydayofferinit' AND $2 matches 'Completed' AND ($16 matches 'mb' OR $16 matches 'gb') AND $20 matches 'Customer');

mpesa_tmp = foreach mpesa_filter generate (chararray) REGEX_EXTRACT($19,'(\\d+).*',1)  as msisdn,
                                    (chararray)SUBSTRING($3,0,8) as date,
                                    (double)$13 as unit;
mpesa_gen_tmp = foreach mpesa_tmp generate ((TRIM(msisdn) matches '^254.*' AND SIZE(TRIM(msisdn)) == 12) ? TRIM(msisdn) : ((TRIM(msisdn) matches '^007.*' AND SIZE(TRIM(msisdn)) == 11) ? CONCAT('254', SUBSTRING(TRIM(msisdn), 2, 11)) : ((TRIM(msisdn) matches '07.*' AND SIZE(TRIM(msisdn)) == 10) ? CONCAT('254',SUBSTRING(TRIM(msisdn), 1, 10)) : ((TRIM(msisdn) matches '7.*' AND SIZE(TRIM(msisdn)) == 9) ? CONCAT('254', TRIM(msisdn)) : 'NA' )))) as msisdn,date,unit;

mpesa_gen = filter mpesa_gen_tmp by not(msisdn == 'NA');

mpesa_grp = group mpesa_gen by (msisdn, date);

mpesa_revenue = foreach mpesa_grp generate flatten(group), (double)com.flytxt.pig.udf.Round((SUM(mpesa_gen.$2) IS NOT NULL ? SUM(mpesa_gen.$2)/100 : 0), 2); 




Total_Data_Revenue = union vou_rev,mgr_rev,mon_rev,data_rev,mpesa_rev;
Data_Revenue_group = group Total_Data_Revenue by (msisdn,date);
Data_Revenue_Gen = foreach Data_Revenue_group generate flatten(group), (double)SUM(Total_Data_Revenue.$2);

---------------DATA-TRIGGER---------------
data_cogrp = COGROUP data_rev by (msisdn, date),
                     mon_rev by (msisdn, date),
                     mgr_rev by (msisdn, date),
                     vou_rev by (msisdn, date),
                     mpesa_rev by (msisdn, date),
					 Data_Revenue_Gen by (msisdn, date),
					 data_usage by (CallingPartyNumber, date),
					 paid_data_usage by (msisdn, date),
					 Bundle_data_usage by (CallingPartyNumber, date),
					 data_usage_2G by (msisdn, date),
					 data_usage_3G by (msisdn, date),
					 data_usage_4G by (msisdn, date);
					 
					 
data_trigger = foreach data_cogrp generate flatten(group), SUM(data_rev.$2),
                                                           SUM(mon_rev.$2),
                                                           SUM(mgr_rev.$2),
                                                           SUM(vou_rev.$2),
                                                           SUM(mpesa_rev.$2),
														   SUM(Data_Revenue_Gen.$2),
														   SUM(data_usage.$2),
														   SUM(paid_data_usage.$2),
														   SUM(Bundle_data_usage.$2),
														   SUM(data_usage_2G.$2),
														   SUM(data_usage_3G.$2),
														   SUM(data_usage_4G.$2);

store data_trigger into '/tmp/data_rev' using PigStorage(',');

