Register /sftp/CIE/WORKFLOWS/UDF/udf.jar;
REGISTER file:/usr/local/pig/contrib/piggybank/java/piggybank.jar;
DEFINE UnixToISO org.apache.pig.piggybank.evaluation.datetime.convert.Unix;


mpesa_load = load '$d1' using org.apache.pig.piggybank.storage.CSVExcelStorage('|');

postpaid_load = load '/ftp-archive/RAW/Postpaid/post_paid*.csv' using org.apache.pig.piggybank.storage.CSVExcelStorage('|') as (postMsisdn:chararray);

rewardedList = load '/ftp-archive/RAW/Rewarded/dummy_*.csv' using org.apache.pig.piggybank.storage.CSVExcelStorage('|') as (rewardMsisdn:chararray);

statusCompleted = filter mpesa_load by $2 == 'Completed' AND $20 == 'Customer';
mpesa_load_tmp = foreach statusCompleted generate (chararray)$1 as recieptNum,(chararray)$2 as status,(chararray)$3 as intitiationTime,(chararray)$5 as tType,(chararray)$12 as channel,((long)$13/100) as amount,(
chararray)$20 as debitPartyType,(chararray) REGEX_EXTRACT($19,'(\\d+).*',1) as msisdn,FLATTEN(STRSPLIT($31,';'));

refDataFiltered = filter mpesa_load_tmp by $20 == 'TerminalType,SafaricomApp';
refDataRemoved = foreach refDataFiltered generate $0 as recieptNum, $1 as status,$2 as intitiationTime,$3 as tType,$4 as channel,$5 as amount,$6 as debitPartyType,((TRIM($7) matches '^254.*' AND SIZE(TRIM($7)) == 12) ? TRIM($7) : ((TRIM($7) matches '^007.*' AND SIZE(TRIM($7)) == 11) ? CONCAT('254', SUBSTRING(TRIM($7), 2, 11)) : ((TRIM($7) matches '07.*' AND SIZE(TRIM($7)) == 10) ? CONCAT('254',SUBSTRING(TRIM($7), 1, 10)) : ((TRIM($7) matches '7.*' AND SIZE(TRIM($7)) == 9) ? CONCAT('254', TRIM($7)) : 'NA' )))) as msisdn,$20 as TerminalType;

msisdn_filtered = filter refDataRemoved by not(msisdn == 'NA');

join_msisdn = JOIN msisdn_filtered by (msisdn) LEFT OUTER, rewardedList by (rewardMsisdn);
filterMsisdn= filter join_msisdn by ($9 is null);

filteredPostpaid = foreach postpaid_load generate((TRIM(postMsisdn) matches '^254.*' AND SIZE(TRIM(postMsisdn)) == 12) ? TRIM(postMsisdn) : ((TRIM(postMsisdn) matches '^007.*' AND SIZE(TRIM(postMsisdn)) == 11) ? CONCAT('254', SUBSTRING(TRIM(postMsisdn), 2, 11)) : ((TRIM(postMsisdn) matches '07.*' AND SIZE(TRIM(postMsisdn)) == 10) ? CONCAT('254',SUBSTRING(TRIM(postMsisdn), 1, 10)) : ((TRIM(postMsisdn) matches '7.*' AND SIZE(TRIM(postMsisdn)) == 9) ? CONCAT('254', TRIM(postMsisdn)) : 'NA' )))) as postpaidMsisdn;


postpaidFiltered = JOIN filterMsisdn by (msisdn) LEFT OUTER, filteredPostpaid by (postpaidMsisdn);

dataToFile = foreach postpaidFiltered generate $0,$1,$2,$3,$4,$5,$6,$7,$8,($10 is not null ? 1:0); 

rmf tmp/MpesaTransact

store dataToFile into 'tmp/MpesaTransact1' using PigStorage(',');

mpesaTransac = load 'tmp/MpesaTransact1/' using PigStorage(',');

mpesaTransac_ranked = rank mpesaTransac;

final = filter mpesaTransac_ranked by (($0) % $Nth == 0);

rmf tmp/Final
store final into 'tmp/Final' using PigStorage(',');

