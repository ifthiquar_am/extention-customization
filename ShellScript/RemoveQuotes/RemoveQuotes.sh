inDir=$1/
outDir=$2/
backUp=$3/
if [ -n "$1" ] || [ -n "$2" ] || [ -n "$3" ];then
for file in $inDir*.*; do  
  fileName=${file##*/}
  ext=${fileName##*.}
  onlyName=$(echo $fileName | cut -f 1 -d '.')
  input=$inDir$onlyName"."$ext
  out="$outDir$onlyName-out.$ext"
  cat $input |sed 's/\""/0/g'|sed 's/\"//g'> $out
done
mv $inDir* $backUp
else
echo "Input, output and backup directories are mandatory"
fi
