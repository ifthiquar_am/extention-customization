import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

class JsonVO {
	private Data[] data;

	public Data[] getData() {
		return data;
	}

	public void setData(Data[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ClassPojo [data = " + data + "]";
	}
}

class Data {
	private String query;

	private String fileName;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "ClassPojo [query = " + query + ", fileName = " + fileName + "]";
	}
}

public class HbasePheonixConnector {

	public static void main(String args[]) throws Exception {
		Connection con;
		ObjectMapper mapper = new ObjectMapper();

		// JSON from file to Object
		JsonVO obj = mapper.readValue(new File("properties.json"), JsonVO.class);
		Data[] data = obj.getData();
		for (Data d : data) {
			String fileStr = d.getQuery();
			String query = d.getFileName();

			if (null == fileStr || null == query) {
				System.out.println("output file path or query is missing in properties");
			}
			System.out.println(query);
			File file = new File(fileStr);

			try {
				Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
				con = DriverManager.getConnection(
						"jdbc:phoenix:omm101.in.nawras.com.om,omm103.in.nawras.com.om,omm104.in.nawras.com.om:2181:/hbase-unsecure");
				System.out.println("Connection Success");
				ResultSet rs = con.createStatement().executeQuery(query);
				try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
					System.out.println("Writing Data to file.....");
					while (rs.next()) {
						out.write(rs.getString(1) + ",");
						out.write(rs.getString(2) + ",");
						out.write(rs.getString(3) + ",");
						out.write(rs.getString(4) + ",");
						out.write(rs.getString(5) + ",");
						out.write(rs.getString(6) + ",");
						out.write(rs.getString(7) + ",");
						out.write(rs.getString(8) + ",");
						out.write(rs.getString(9));
						out.newLine();
					}
					System.out.println("Completed writing into text file");
				}
			} catch (Exception e) { // TODO Auto-generated catch block e.printStackTrace();

			}
		}

	}

}