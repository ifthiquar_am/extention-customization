package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Properties;

public class HbaseTest {

	public static void main(String args[]) {
		 Connection con;
		Properties p = new Properties();
		FileReader reader;
		try {
			reader = new FileReader("hbasepheonix.properties");
			p.load(reader);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String fileStr = p.getProperty("outputfile");
		String query = p.getProperty("query");
		if (null == fileStr || null == query) {
			System.out.println("output file path or query is missing in properties");
		}
		System.out.println(query);
		File file = new File(fileStr);

		try {
			Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
			con = DriverManager.getConnection(
					"jdbc:phoenix:omm101.in.nawras.com.om,omm103.in.nawras.com.om,omm104.in.nawras.com.om:2181:/hbase-unsecure");
			System.out.println("Connection Success");
			ResultSet rs = con.createStatement().executeQuery(
					"SELECT SUBSCRIBER_NUM, CUSTOMER_ID, SUBSCRIPTION_TYPE, SUBSCRIPTION_STATUS, SUBSCIPTION_OFFERING, ACTIVATION_TYPE, ACTIVATION_DTTM, USAGE_SEGMENTATION, CUSTOMER_AVERAGE FROM EIF.SUBSCRIPTION limit 5");
			try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
				System.out.println("Writing Data to file.....");
				while (rs.next()) {
					out.write(rs.getString(1) + ",");
					out.write(rs.getString(2) + ",");
					out.write(rs.getString(3) + ",");
					out.write(rs.getString(4) + ",");
					out.write(rs.getString(5) + ",");
					out.write(rs.getString(6) + ",");
					out.write(rs.getString(7) + ",");
					out.write(rs.getString(8) + ",");
					out.write(rs.getString(9));
					out.newLine();
				}
				System.out.println("Completed writing into text file");
			}
		} catch (Exception e) { // TODO Auto-generated catch block e.printStackTrace();

		}

	}

}
