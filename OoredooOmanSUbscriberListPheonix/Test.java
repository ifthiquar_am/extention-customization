package com.flytxt.services.TestBootApp.test;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

class JsonVO {
	private Data[] data;

	public Data[] getData() {
		return data;
	}

	public void setData(Data[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ClassPojo [data = " + data + "]";
	}
}

class Data {
	private String query;

	private String fileName;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "ClassPojo [query = " + query + ", fileName = " + fileName + "]";
	}
}

public class Test {

	public static void main(String[] args) throws Exception {
		ObjectMapper mapper = new ObjectMapper();

		// JSON from file to Object
		JsonVO obj = mapper.readValue(new File("test.json"), JsonVO.class);
		Data[] data = obj.getData();
		for(Data d:data) {
			System.out.println(d.getQuery());
			System.out.println(d.getFileName());
		}
	}
}
